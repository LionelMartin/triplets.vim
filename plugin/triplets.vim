if exists ("g:loaded_triplets")
    finish
endif
let g:loaded_triplets = 1

command! -nargs=0 Triplets call triplets#MakeNumberReadable()
