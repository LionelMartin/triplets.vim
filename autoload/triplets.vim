function! triplets#MakeNumberReadable() abort
  let number = expand('<cword>')
  if number !~ '^\d\+$' | return | endif

  let saved_view = winsaveview()
  defer winrestview(saved_view)

  let groups = []
  let current_group = ''
  let current_count = 0

  for digit in reverse(split(number, '\zs'))
    let current_group = digit .. current_group
    let current_count += 1

    if current_count == 3
      call add(groups, current_group)
      let current_count = 0
      let current_group = ''
    endif
  endfor

  if current_count > 0
    call add(groups, current_group)
  endif

  exe 'normal! "_ciw' .. join(reverse(groups), '_')
endfunction
