# triplets.vim

Vim plugin making numbers more readable by changing `1000000` to `1_000_000`.

A lot of programming languages accept using `_` as separator in numbers (like go, javascript, typescript, python, C#, Swift, ada...).

This is a port of [readable-number.nvim](https://github.com/asrul10/readable-number.nvim) to vimscript originally posted in a [reddit thread](https://www.reddit.com/r/neovim/comments/10ucjmo/comment/j7f5orh/) by [Andrew Radev](https://andrewra.dev/).

### Installation
* With [Pathogen](http://www.vim.org/scripts/script.php?script_id=2332)
```bash
cd ~/.vim/bundle
git clone https://codeberg.org/LionelMartin/triplets.vim.git
```

* With [Vundle](https://github.com/gmarik/vundle)
```vimrc
" .vimrc
Bundle 'https://codeberg.org/LionelMartin/triplets.vim.git'
```
* With [minpac](https://github.com/k-takata/minpac)
```vimrc
" .vimrc
call minpac#add('https://codeberg.org/LionelMartin/triplets.vim.git')
```
### Usage

```vimrc
	nnoremap <leader>tr :Triplets<cr>
```
